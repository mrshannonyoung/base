#Hi. I'm [Shannon Young](http://shannonyoung.co.uk), I work for [mark-making*](http://mark-making.com).

##Don't really know what to say yet
I suppose this is a base to start projects using Grunt and just a good reference

##Features:

###[Grunt](http://gruntjs.com/) tasks
* Watch
* CSS linting
* Compass
* JS hint
* Browser sync

###[Bower](http://bower.io/)
* [jQuery](http://jquery.com/)
* [Modernizr](http://modernizr.com/)

###Markup
* Micro-data on elements
* [ARIA roles](http://www.w3.org/TR/wai-aria/roles#role_definitions) on elements

##Things to do
* ~~Strikethrough something~~
* Decide on Grid system
* Use a **[BEM](http://api.yandex.com/bem/ "BEM")** or **[SMACSS](http://smacss.com/ "SMACSS")** methodology.


