'use strict';
module.exports = function(grunt) {

  // load all grunt tasks matching the `grunt-*` pattern
  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig (
  {
    pkg: grunt.file.readJSON('package.json'),

    watch: {
      sass: {
        files: ['assets/sass/**/*.{scss,sass}'],
        tasks: ['compass']
      },
      js: {
        files: ['assets/js/*.js','Gruntfile.js'],
        tasks: ['jshint']
      },      
      lint: {
        files: ['assets/css/*.css'],
        tasks: ['csslint']
      }
    },

    compass: {
      dist: {
        options: {
          sassDir: 'assets/sass',
          cssDir: 'assets/css',
          outputStyle: 'nested'
        }
      }
    },

    csslint: {
      options: {
        // External file
        csslintrc: '.csslintrc'
      },
      strict: {
        options: {
          import: 2
        },
        src: ['assets/css/*.css']
      },
      lax: {
        options: {
          import: false
        },
        src: ['assets/css/*.css']
      }
    },

    jshint: {
      options: {
        // External file
        jshintrc: '.jshintrc'
      },
      all: ['assets/js/*.js']
    },

    browser_sync: {
        dev: {
            bsFiles: {
                src : ['assets/css/root.css', '*.html']
            },
            options: {
                watchTask: true,
                proxy: {
                    // Your existing vhost setup
                    host: '192.168.1.150',
                    port: 8888
                }
            }
        }
    },

    'ftp-deploy': {
      build: {
        auth: {
          host: '91.208.99.4',
          port: 21,
          authKey: 'key1'
        },
        src: [
          'test'
        ],
        dest: '/public_html/test',
        exclusions: [
          '.DS_Store',
          'Thumbs.db',
          '.git*'
        ]
      }
    }

  }
  );

  // Register the default tasks.
  grunt.registerTask('default', ['browser_sync','watch']);
};